//! Datatypes and utilities related to the different possible filetypes specified by the tar
//! archive format's typeflag field.

/// Every filetype supported by the typeflag of the tar archive header format.
/// The "reserved" type is not supported and should be considered an error.
pub enum FileType {
    Regular,
    HardLink,
    SoftLink,
    CharacterSpecial,
    BlockSpecial,
    Directory,
    Fifo,
    LongLink,
}

impl std::fmt::Debug for FileType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        let s = match self {
            FileType::Regular => "regular",
            FileType::HardLink => "hard link",
            FileType::SoftLink => "soft link",
            FileType::CharacterSpecial => "character special",
            FileType::BlockSpecial => "block special",
            FileType::Directory => "directory",
            FileType::Fifo => "fifo",
            FileType::LongLink => "long link",
        };
        f.write_str(s)?;
        Ok(())
    }
}

impl std::fmt::Display for FileType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        let s = match self {
            FileType::Regular => "-",
            FileType::HardLink => "h",
            FileType::SoftLink => "l",
            FileType::CharacterSpecial => "c",
            FileType::BlockSpecial => "b",
            FileType::Directory => "d",
            FileType::Fifo => "p",
            FileType::LongLink => "L",
        };
        f.write_str(s)?;
        Ok(())
    }
}

/// Creates a FileType enum directly from the typeflag of a tar header.
impl<'a> std::convert::From<char> for FileType {
    fn from(s: char) -> FileType {
        match s {
            '0' => FileType::Regular,
            '\0' => FileType::Regular,
            '1' => FileType::HardLink,
            '2' => FileType::SoftLink,
            '3' => FileType::CharacterSpecial,
            '4' => FileType::BlockSpecial,
            '5' => FileType::Directory,
            '6' => FileType::Fifo,
            //'7' => FileType::Reserved,
            'L' => FileType::LongLink,
            _ => {
                panic!("Unhandled typeflag: {}", s);
            }
        }
    }
}
